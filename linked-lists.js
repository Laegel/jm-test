/* INIT */
const item1 = {val: 52, next: null};
const item2 = {val: 32, next: null};
const item3 = {val: 16, next: null};
const item4 = {val: 42, next: null};
const item5 = {val: 95, next: null};
const item6 = {val: 456, next: null};
const item7 = {val: 7, next: null};
const item8 = {val: 332, next: null};

item1.next = item2;
item2.next = item3;
item3.next = item4;
item4.next = item5;
item5.next = item6;
item6.next = item7;
item7.next = item8;

/* FUNCTIONS */
function walkLinkedList(input, callback) {
	let tmp = input;
	callback(tmp);
	while (tmp && tmp.next) {
		tmp = tmp.next;
		callback(tmp);
	}
}

function reverseLinkedList(input) {
	if (!input) {
		return null;
	}
	let current = input;
	let next = current.next;
	current.next = null;
	let tmp;
	while (next) {
		tmp = next.next;
		next.next = current;
		current = next;
		next = tmp;
	}
	return current;
}

function reverseLinkedListRecurs(input) {
	if (!input || !input.next) {
		return input;
	}
	const current = reverseLinkedListRecurs(input.next);
	input.next.next = input;
	input.next = null;
	return current;
}

/* PROCESS */
let list;
if (process.argv[2] && 'recurs' === process.argv[2]) {
	console.log('Calling recursive mode');
	list = reverseLinkedListRecurs(item1);
} else if (process.argv[2] && 'linear' === process.argv[2]) {
	console.log('Calling linear mode');
	list = reverseLinkedList(item1);
} else {
	console.log('Displaying linked list');
	list = item1;
}

walkLinkedList(list, item => {
	console.log(item.val);
});