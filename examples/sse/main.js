const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const arabicToRoman = require('../convert.js');
const numberFile = 'number.txt';

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'), {encoding: 'utf-8'});
});

app.get('/converter', (req, res) => {
	res.writeHead(200, {
    	'Content-Type': 'text/event-stream',
    	'Cache-Control': 'no-cache',
    	'Connection': 'keep-alive'
  	});

  	var id = (new Date()).toLocaleTimeString();

  	setInterval(function() {
  		try {
  			fs.access(numberFile, (err) => {
				if (err && err.code) {
					return;
				}
	    		constructSSE(res, id, arabicToRoman(fs.readFileSync(numberFile, {encoding: 'utf-8'})));
			});
  		} catch (err) {}
  	}, 250);
});

function constructSSE(res, id, data) {
  	res.write('id: ' + id + '\n');
  	res.write('data: ' + data + '\n\n');
  	fs.unlinkSync(numberFile);
}

app.get('/register', (req, res) => {
	fs.writeFileSync(numberFile, req.query.number);
	res.end();
});

app.listen(8080);
