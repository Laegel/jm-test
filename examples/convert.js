function arabicToRoman(input) {
	var map = {
		'M': 1000, 'CM': 900, 'D': 500, 
		'CD': 400, 'C': 100, 'XC': 90, 
		'L': 50, 'XL': 40, 'X': 10, 
		'IX': 9, 'V': 5, 'IV': 4, 'I': 1
	};
	var number = input;
	var output = '';
	var i;
	var timesFound;
	var rest;
	var divider;
	for (roman in map) {
		divider = map[roman];
		timesFound = number / divider;
		rest = number % divider;
		if (timesFound >= 1) {
			number = rest;
			output += roman.repeat(timesFound);
		}
	}
	return output;
}

module.exports = arabicToRoman;