const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const arabicToRoman = require('../convert.js');

app.get('/', (req, res) => {
	let result = '';
	let number = 1;
	if (req.query && req.query.number) {
		result = arabicToRoman(req.query.number);
		number = req.query.number;
	}
    res.send(fs.readFileSync(path.join(__dirname + '/index.html'), {encoding: 'utf-8'})
    	.replace('{{result}}', result)
    	.replace('{{number}}', number)
    );
}).listen(8080);